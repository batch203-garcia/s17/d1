/* console.log("Hello, World!"); */

//[SECTION] Functions
//lines/blocks of codes that performs a certain task when called.


//function declaration -> syntax: function functionName(){ codeblock }

function printName() {
    console.log("My name is John")
}

//[SECTION] Function Invocation -> call or invoke a function

printName();


function declaredFunction() {
    console.log("Hello World!")
}

declaredFunction();

//[SECTION] Function Declaration vs Expression
//function declaration
//can be created within a function using function keyword and function name.

//function expression
//can be stored in a variable.
//is an anonymous function -> without a name

//variableFunction(); -> error function expression -> cannot be hoisted

let variableFunction = function() {
    console.log("Hello Again!")
}

variableFunction();


//can
let funcExpression = function funcName() {
    console.log("Hello from the other side.")
}

funcExpression();
//funcName(); -> error

declaredFunction();

declaredFunction = function() {
    console.log("updated declared function.")
}

declaredFunction();

funcExpression();

funcExpression = function() {
    console.log("updated function expression");
}

funcExpression();

const constantFunc = function() {
    console.log("Initialized with const");
}

constantFunc();

/* 
const function value cannot be changed

constantFunc = function() {
    console.log("New Value");

    //constantFunc(); -> error
}
*/

// [SECTION] Function Scooping

/* 

the accessability and visibility of a variable

3 types of scoop
1. local scope
2. global scope
3. function scope

*/

//variables inside {} can only access locally.
{
    let localVar = "Armando Perez";
    console.log(localVar);
}

let globalVar = "Mr. Worldwide"
console.log(globalVar);

console.log(globalVar);
// console.log(localVar); //result in error.

// Function Scopes 
// Javascript has function scopes: Each function creates a new scope.
//  Variables defined inside a function are not accessible/visible outside the function.
// Variables declared with var, let, and const are quite similar when declared inside a function.

function showNames() {
    //Function scoped variables:
    var functionVar = "Joe";
    const functionConst = "John";
    let functionLet = "Jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showNames();
// Error - This are function scoped variable and cannot be accessed outside the function they were declared in.
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

// Nested Functions
// You can create another function inside a function.
// This is called a nested function.

function myNewFunction() {
    let name = "Jane";

    function nestedFunction() {
        let nestedName = "John"
        console.log(name);
    }

    // console.log(nestedName); // result to not defined error.
    nestedFunction();
}

myNewFunction();
// nestedFunction(); // result to not defiend error.


// Function and Global Scope variables

// Global Scoped Variable
let globalName = "Alexandro";

function myNewFunction2() {
    let nameInside = "Renz";
    console.log(globalName);
}

myNewFunction2();
// console.log(nameInside);

//[SECTION] Using alert() -> small pop-up window

// alert("Hello World!"); -> will run immediately

function showSampleAlert() {
    alert("Hello World!")
}

//showSampleAlert();

console.log("I will only log when alert is dismissed.");

//[SECTION] Using promt() -> samll pop-up windows -> accepts user input
//Syntax -> let variableName = prompt("<dialogInString>");

// let samplePromt = prompt("Enter your Full Name");
// console.log(typeof samplePromt);
// console.log("Hello, " + samplePromt + "!");

//this prompt() returns empty string -> null if canceled.
//let sampleNullPrompt = prompt("don't enter anything");
//console.log(sampleNullPrompt);

//function scoped -> store data from prompt()

function printWelcomeMessage() {
    let firstName = prompt("Enter your first name: ");
    let lastName = prompt("Enter your last name: ");

    console.log("Hello, " + firstName + " " + lastName + "!");
    console.log("Welcome to my page!")
}

printWelcomeMessage();

//[SECTION] Function Naming Convention

//Function names should be definitive
function getCourses() {
    let courses = ["Science 101", "Math 101", "English 101"];
    console.log(courses);
}

getCourses();

//avoid generic names
function get() {
    let name = "Jamie"
    console.log(name);
}

get();

//avoid pointless function names -> like foo, bar
function foo() {
    console.log(25 % 5)
}

foo();

//Name functions in small caps -> or follow camelCase
function displayCarInfo() {
    console.log("Brand: Toyota");
    console.log("type: Sedan");
    console.log("Price: 1, 500, 000");
}

displayCarInfo();